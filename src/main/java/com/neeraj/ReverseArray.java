package com.neeraj;

import java.util.Arrays;

public class ReverseArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] input = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		System.out.println(Arrays.toString(input));
		System.out.println(Arrays.toString(reverseString(input)));

	}

	private static Integer[] reverseString(Integer[] input) {

		int arrayLength = input.length;

		for (int i = 0; i < arrayLength / 2; i++) {
			int tempFirst = i;
			int tempLast = arrayLength - i - 1;

			input[tempFirst] = input[tempLast] + input[tempFirst]; // a = a+b (9 = 5+4)
			input[tempLast] = input[tempFirst] - input[tempLast]; // b = a-b; (5 = 9-4)
			input[tempFirst] = input[tempFirst] - input[tempLast]; // b = a-a (4 = 9-5)

		}

		return input;
	}

}
