package com.neeraj;

import java.util.ArrayList;
import java.util.List;

public class ComputerUsage {

    public static void main(String[] args) {
        int deviceCapacity = 7;
        List<List<Integer>> foregroundAppList = new ArrayList<>();
        List<List<Integer>> backgroundAppList = new ArrayList<>();

        List<Integer> temp = new ArrayList<>();
        temp.add(1);
        temp.add(2);

        foregroundAppList.add(temp);

        temp = new ArrayList<>();
        temp.add(2);
        temp.add(4);
        foregroundAppList.add(temp);

        temp = new ArrayList<>();
        temp.add(3);
        temp.add(6);
        foregroundAppList.add(temp);

        temp = new ArrayList<>();
        temp.add(1);
        temp.add(2);
        backgroundAppList.add(temp);

        getOptumPares(deviceCapacity, foregroundAppList,backgroundAppList);
    }

    private static List<List<Integer>> getOptumPares(int deviceCapacity, List<List<Integer>> foregroundAppList, List<List<Integer>> backgroundAppList) {

        List<List<Integer>> validForground = getValidList(foregroundAppList, deviceCapacity);
        List<List<Integer>> validBackGround = getValidList(backgroundAppList, deviceCapacity);

        return getTheCombination(validBackGround,validForground,deviceCapacity);

    }

    private static List<List<Integer>> getTheCombination(List<List<Integer>> validBackGround, List<List<Integer>> validForground, int deviceCapacity) {

        List<List<Integer>> output = new ArrayList<>();
        List<List<Integer>> optimalUse = new ArrayList<>();

        for(int i = 0 ; i< validForground.size(); i++){

            for (int j = 0 ; j < validBackGround.size() ; j++){
                int tempSum = validForground.get(i).get(1) + validBackGround.get(j).get(1);
                if ( tempSum == deviceCapacity ){

                    output.add(getAddedValue(validForground.get(i), validBackGround.get(j)));
                }else if ((( tempSum + 1) == deviceCapacity)){
                    optimalUse.add(getAddedValue(validForground.get(i), validBackGround.get(j)));
                }
            }

        }

//        System.out.println("output : " + output);

        if (output.size() == 0) {
            return optimalUse;
        }
        return output;
    }

    private static List<Integer> getAddedValue(List<Integer> integers, List<Integer> integers1) {
        List<Integer> newCombination = new ArrayList<>();
        newCombination.add(integers.get(0));
        newCombination.add(integers1.get(0));
        return newCombination;
    }

    private static List<List<Integer>> getValidList(List<List<Integer>> foregroundAppList, int deviceCapacity) {
        List<List<Integer>> output = new ArrayList<>();

        for (List<Integer> values: foregroundAppList) {
            if (values.get(1) <= deviceCapacity){
                output.add(values);
            }
        }

        return output;
    }
}
