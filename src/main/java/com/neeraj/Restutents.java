package com.neeraj;

import java.util.*;

public class Restutents {

    public static void main(String[] args) {
        int numOfRest = 3;
        List<List<Integer>> allLocation = new ArrayList<>();

        List<Integer> temp = new ArrayList<>();
        temp.add(1);
        temp.add(2);

        allLocation.add(temp);

        temp = new ArrayList<>();
        temp.add(200);
        temp.add(-199);
        allLocation.add(temp);

        temp = new ArrayList<>();
        temp.add(20);
        temp.add(-1);
        allLocation.add(temp);

        int numberOfOutput = 2;

        System.out.println(getNearestResturents(numOfRest,allLocation,numberOfOutput));
    }

    private static List<List<Integer>> getNearestResturents(int numOfRest, List<List<Integer>> allLocation, int numberOfOutput) {

        SortedMap<Integer, List<Integer>> mapping = new TreeMap<>();

        for (int i = 0 ; i < allLocation.size() ; i++){
            mapping.put(getDistance(allLocation.get(i)),allLocation.get(i));
        }

        return getNearestLocation(mapping, numberOfOutput);
    }

    private static List<List<Integer>> getNearestLocation(SortedMap<Integer, List<Integer>> mapping, int maxNearestRest) {
        int i = 1;
        List<List<Integer>> output = new ArrayList<>();

        for (Map.Entry<Integer, List<Integer>> e: mapping.entrySet()) {
            output.add(e.getValue());
            if (i == maxNearestRest){
                return output;
            }else {
                i++;
            }
        }
        return output;
    }

    private static Integer getDistance(List<Integer> integers) {
        return ((integers.get(1) * integers.get(1)) + ((integers.get(0) * integers.get(0) )));
    }
}
