package com.neeraj;

import java.util.*;

/**
 * Hello world!
 */
public class AmazonHouseAssignment {
    public static void main(String[] args) {

        Integer[] input = {1, 0, 0, 0, 0, 1, 0, 0};
        int noofdays = 1;

        List<Integer> integerList= Arrays.asList(input);

        System.out.printf("Input value is :\t" + integerList);

        System.out.printf("\nOutput value is :\t" + cellCompete(input,noofdays).toString());
    }

    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    public static List<Integer> cellCompete(Integer[] states, int days) {
        // WRITE YOUR CODE HERE
        List<Integer> output = new ArrayList<>();

        for (int i = 0; i < states.length; i++) {
            int previousValue = 0;
            int nextValue = 0;
            int next2NextValue = 0;
            int currentValue = states[i];

            if (i == 0) {
                previousValue = 0;
                nextValue = states[i + 1];
                next2NextValue = states[i + 2];
            } else if (i == states.length - 2) {

                previousValue = states[i - 1];
                nextValue = states[i + 1];
                next2NextValue = 0;

            } else if (i == states.length - 1) {

                previousValue = states[i - 1];
                nextValue = 0;
                next2NextValue = 0;

            } else {

                previousValue = states[i - 1];
                nextValue = states[i + 1];
            }
            int result = findTheCurrentValueStatus(previousValue, currentValue, nextValue, next2NextValue);
            System.out.println("a[" + i + "] : " + currentValue + ". Output is:" + result);
            output.add(result);
        }
        return output;

    }

    public static int findTheCurrentValueStatus(int pre, int current, int next, int next2Next) {

        System.out.printf("pre %s pre", pre, pre);
        if (current == pre && current == next) {
            return 0;
        } else {
            return 1;
        }
    }
    // METHOD SIGNATURE ENDS
}
