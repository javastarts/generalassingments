package com.neeraj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupingNumber {

	public static void main(String[] args) {
		Integer[] input = { 3, 3, 8, 8, 8, 7, 7, 7, 7 };

		System.out.println(groupNumbers(input));

	}

	private static List<Integer> groupNumbers(Integer[] input) {

		Map<Integer, Integer> groups = new HashMap<>();

		for (int i = 0; i < input.length; i++) {

			if (groups.containsKey(input[i])) {
				int temp = groups.get(input[i]);

				groups.put(input[i], temp + 1);
			} else {
				groups.put(input[i], 1);
			}
		}

		List<Integer> output = new ArrayList<Integer>();

		for (java.util.Map.Entry<Integer, Integer> entry : groups.entrySet()) {
			output.add(entry.getValue());
			output.add(entry.getKey());
		}

		return output;

	}

}
