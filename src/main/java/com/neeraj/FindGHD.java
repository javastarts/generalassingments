package com.neeraj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FindGHD {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        int[] inputarray = {11,121};
        int count = inputarray.length;
        System.out.println("args = [" + getGCD(count, inputarray) + "]");
    }

    public static int getGCD(int numbercount, int[] inputArray) {

        Set<Integer> highestDividentsOfAll = new HashSet<>();
        int highestDivident = 1;

        for (int i = 0; i < numbercount; i++) {
            int temp = getHighesDivident(inputArray[i]);
            highestDividentsOfAll.add(temp);
            System.out.println("finalHighestDivident value for " + inputArray[i] + " is:" + temp);
        }
        return getCommonHighestDivident(highestDividentsOfAll,inputArray);
    }

    public static int getCommonHighestDivident(Set<Integer> dividents, int[] inputArray) {
        return dividents.stream()
                .filter(d -> {
                    boolean correctDivident = true;
                    for (int i = 0; i < inputArray.length; i++) {
                        if ((inputArray[i] % d) != 0) {
                            return false;
                        }
                    }
                    return correctDivident;
                })
                .max((i1, i2) -> i1>i2 ? i1 : i2)
                .get();
    }

    public static int getHighesDivident(int number) {
        int hightDivident = 0;
        for (int i = 1; i <= 12; i++) {
            if (number % i == 0) {
                hightDivident = i;
            }
            System.out.println("\tnumber = [" + number + "]'s hightDivident is:" + hightDivident);
        }
        return hightDivident;
    }
}
