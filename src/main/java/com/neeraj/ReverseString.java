package com.neeraj;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ReverseString {

	static int maximumValue = 100;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		int numberOfIntegers = Integer.parseInt(br.readLine());
		int numberOfIntegers = Integer.parseInt(br.readLine().trim());

		if (numberOfIntegers > 0 && numberOfIntegers <= 100) {
			for (int i = 1; i <= numberOfIntegers; i++) {
				String input = br.readLine();
				System.out.println(processStringLine(input));
			}
		}
	}

	private static boolean checkMaxiMumValue(int count) {
		if (count > 0 && count <= maximumValue) {
			return true;
		}
		return false;
	}

	private static String processStringLine(String input) {
		if (checkMaxiMumValue(input.length())) {
			StringBuilder output = new StringBuilder(" ");
			String temp[] = input.split(" ");

			for (int i = 0; i < temp.length; i++) {
				output.append(reverseString(temp[i]));
			}
			return output.toString().trim();
		}
		return null;
	}

	private static String reverseString(String inputWord) {
		if (checkMaxiMumValue(inputWord.length())) {
			StringBuilder outputWord = new StringBuilder(" ");
			char inputchararray[] = inputWord.toCharArray();

			for (int i = inputchararray.length - 1; i >= 0; i--) {

				char temp = inputchararray[i];
				outputWord.append(temp);
			}
			return outputWord.toString();
		}
		return null;
	}
}