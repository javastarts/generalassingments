package com.neeraj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//List of emp opbject. Sorted based on cities.

public class Example {

	public static void main(String[] args) {
		List<Emp> emps = new ArrayList<>();

		Emp emp1 = new Emp(1, "Neeraj", "Arizona");
		Emp emp2 = new Emp(2, "Rohit", "Texas");
		Emp emp3 = new Emp(3, "Hemanth", "California");

		emps.add(emp2);
		emps.add(emp1);
		emps.add(emp3);

		Collections.sort(emps, (e1, e2) -> e1.getCity().compareTo(e2.getCity()));

		System.out.println(emps);
	}
}

class Emp {

	int empid;
	String empName;
	String city;

	public Emp(int empid, String empName, String city) {
		super();
		this.empid = empid;
		this.empName = empName;
		this.city = city;
	}

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Emp [empid=" + empid + ", empName=" + empName + ", city=" + city + "]";
	}
}